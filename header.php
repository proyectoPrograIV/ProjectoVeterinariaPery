<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 

    <title>Veterinaria</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css1/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Custom CSS -->
    <link href="assets/css1/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
<a href="header.php"></a>
 
</head>

<body>

  
    <nav class="navbar navbar-default" role="navigation">
        	<?php

                        require_once("Login/session.php");

                        require_once("Login/usuario.php");
                        $usuario = new Usuario();


                        $idUsuario = $_SESSION['sesion'];
                        $datosU=$usuario->getUsuario($idUsuario);

                ?>
			
             <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                   
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="index.php">Veterinaria Pery</a>
            </div>
			 
          </a>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
                <ul class="nav navbar-nav">
                      <li>
                        <a href="home.php"><span class="glyphicon glyphicon-home"></span>Inicio</a>
                    </li>
					 <li>
                        <a href="clientes.php"><span class="glyphicon glyphicon-user">Clientes</a>
                    </li>
                    <li>
                        <a href="Medicament.php"><span class="glyphicon glyphicon-list-alt">Medicacion</a>
                    </li>
					<li>
                        <a href="Mascots.php"><span class=" glyphicon glyphicon-certificate">Mascota</a>
                    </li>
                      <li>
					<li>
                        <a href="service.php"><span class=" glyphicon glyphicon-file">Servicios</a>
                    </li>
                      <li>
					<li>
                        <a href="users.php"><span class=" glyphicon glyphicon-user">Usuarios</a>
                    </li>
                     <li>
					<li>
                        <a href="visit.php"><span class=" glyphicon glyphicon-bell">Visitas</a>
                    </li>
                    
                    
                    
                    
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">User:  <?php echo $datosU['nomUsuario']; ?>
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="perfil.php">Perfil</a></li>  
                          
                          <li><a href="Login/logout.php?logout=true">Cerrar Sesion</a></li>                         
                        </ul> 
                    </li>
                     <li>
					<li>
                        <a href="help.php"><span class="glyphicon glyphicon-question-sign">Ayuda</a>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
