<?php

include_once 'database.php';

class Usuario
{	

	private $pdo;
	
	public function __construct()
	{
		$db = Database::Conectar();
		$this->pdo = $db;
        }
	
	public function consulta($sql)
	{
		$stmt = $this->pdo->prepare($sql);
		return $stmt;
	}
	
        public function registrar($nomUsuario,$emailUsuario,$passUsuario)
	{
		try
		{
			$nuevoPassword = password_hash($passUsuario, PASSWORD_DEFAULT);
			
			$stmt = $this->pdo->prepare("INSERT INTO usuario(nomUsuario,emailUsuario, passUsuario) 
		                                               VALUES(:nomUsuario, :emailUsuario, :passUsuario)");
												  
			$stmt->bindparam(":nomUsuario", $nomUsuario);
			$stmt->bindparam(":emailUsuario", $emailUsuario);
			$stmt->bindparam(":passUsuario", $nuevoPassword);									  
				
			$stmt->execute();	
			
			return $stmt;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}				
	}
	public function ListarUsuarios($nomUsuario, $emailUsuario)
	{
		try
		{
			$stmt = $this->pdo->prepare("SELECT nomUsuario, emailUsuario FROM usuario WHERE nomUsuario=:nomUsuario OR emailUsuario=:emailUsuario");
			$stmt->execute(array(':nomUsuario'=>$nomUsuario, ':emailUsuario'=>$emailUsuario));

			return $datosU=$stmt->fetch(PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
        public function getUsuario($idUsuario)
	{
		try
		{
			$stmt = $this->pdo->prepare("SELECT idUsuario, nomUsuario, emailUsuario,passUsuario FROM usuario WHERE idUsuario=:idUsuario");
			$stmt->execute(array(':idUsuario'=>$idUsuario));

			return $datosU=$stmt->fetch(PDO::FETCH_ASSOC);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function login($nombre,$correo,$clave)
	{
		try
		{
			$stmt = $this->pdo->prepare("SELECT idUsuario, nomUsuario, emailUsuario, passUsuario FROM usuario WHERE nomUsuario=:nombre OR emailUsuario=:correo ");
			$stmt->execute(array(':nombre'=>$nombre, ':correo'=>$correo));
			$datosU=$stmt->fetch(PDO::FETCH_ASSOC);
			if($stmt->rowCount() == 1)
			{
				if(password_verify($clave, $datosU['passUsuario']))
				{
					$_SESSION['sesion'] = $datosU['idUsuario'];
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function is_loggedin()
	{
		if(isset($_SESSION['sesion']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function doLogout()
	{
		session_destroy();
		unset($_SESSION['sesion']);
		return true;
	}
}
?>

