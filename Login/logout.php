<?php
	require_once('session.php');
	require_once('usuario.php');
	$usuario = new Usuario();
	
	if($usuario->is_loggedin()!="")
	{
		$usuario->redirect('../home.php');
	}
	if(isset($_GET['logout']) && $_GET['logout']=="true")
	{
		$usuario->doLogout();
		$usuario->redirect('../index.php');
	}
