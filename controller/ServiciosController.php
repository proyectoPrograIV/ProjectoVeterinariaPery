

<?php

require_once '../../model/servicios/Serviciosmodel.php';

class ServiciosController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Servicios();
    }    
    public function Index(){
        require_once '../../view/servicios/Servicios.php';
       
    }    
    public function Crud(){
        $data = new Servicios();
        
        if(isset($_REQUEST['ID_servicio'])){
            $data = $this->model->getByID($_REQUEST['ID_servicio']);
            
        }       
        require_once '../../view/servicios/EditarServicios.php';          
    }  
    public function add(){
        $data = new Servicios();
        $data->ID_servicio = $_REQUEST['ID_servicio'];
        $data->Tipo_servicio = $_REQUEST['Tipo_servicio'];
        
        $data->ID_servicio > 0
            ? $this->model->update($data)
            : $this->model->add($data);
        header('Location: ../../view/servicios/index.php');
    }
    
     public function Del(){
        $data = new Servicios();
        
         $data->ID_servicio = $_REQUEST['ID_servicio'];
        $data->Tipo_servicio = $_REQUEST['Tipo_servicio'];

        $data->ID_servicio > 0
            ? $this->model->del($data)
            : $this->model->update($data);
        header('Location: index.php');
    }
    
 
   
}

