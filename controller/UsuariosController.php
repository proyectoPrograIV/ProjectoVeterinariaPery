

<?php

require_once '../../model/usuarios/Usuariosmodel.php';

class UsuariosController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Usuarios();
    }    
    public function Index(){
        require_once '../../view/usuarios/Usuarios.php';
       
    }    
    public function Crud(){
        $data = new Usuarios();
        
        if(isset($_REQUEST['Id_Usuario'])){
            $data = $this->model->getByID($_REQUEST['Id_Usuario']);
            
        }       
        require_once '../../view/usuarios/Editarusuarios.php';          
    }  
    public function add(){
        $data = new Usuarios();
        
        $data->Id_Usuario = $_REQUEST['Id_Usuario'];
        $data->Usuario = $_REQUEST['Usuario'];
        $data->Password = $_REQUEST['Password'];
		$data->Privilegio = $_REQUEST['Privilegio'];
        

        $data->Id_Usuario > 0
            ? $this->model->update($data)
            : $this->model->add($data);
        header('Location: ../../view/usuarios/index.php');
    }
    
     public function Del(){
        $data = new Usuarios();
        
        $data->Id_Usuario = $_REQUEST['Id_Usuario'];
        $data->Usuario = $_REQUEST['Usuario'];
        $data->Password = $_REQUEST['Password'];
		$data->Privilegio = $_REQUEST['Privilegio'];

        $data->Id_Usuario > 0
            ? $this->model->del($data)
            : $this->model->update($data);
        header('Location: index.php');
    }
    
 
   
}

