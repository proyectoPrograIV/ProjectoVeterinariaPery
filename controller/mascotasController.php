

<?php

require_once '../../model/Mascota/mascotamodel.php';

class mascotasController{
    
    private $model;
    
    public function __construct(){
        $this->model = new mascota();
    }    
    public function Index(){
        require_once '../../view/Mascotas/mascotas.php';
       
    }    
    public function Crud(){
        $data = new mascota();
        
        if(isset($_REQUEST['Id_mascota'])){
            $data = $this->model->getByID($_REQUEST['Id_mascota']);
            
        }       
        require_once '../../view/Mascotas/Editarmascota.php';          
    }  
    public function add(){
        $data = new mascota();
        
        $data->Id_mascota = $_REQUEST['Id_mascota'];
        $data->ID_Cliente = $_REQUEST['ID_Cliente'];
        $data->Nombre_mascota = $_REQUEST['Nombre_mascota'];
        $data->Tipo_mascota= $_REQUEST['Tipo_mascota'];
        $data->fecha_nacimiento = $_REQUEST['fecha_nacimiento'];
        $data->Sexo = $_REQUEST['Sexo'];
        $data->Peso = $_REQUEST['Peso'];
        $data->Ultima_visita = $_REQUEST['Ultima_visita'];
		$data->Vacunas = $_REQUEST['Vacunas'];
		$data->Observaciones = $_REQUEST['Observaciones'];
		
		

        $data->Id_mascota > 0
            ? $this->model->update($data)
            : $this->model->add($data);
        header('Location:../../view/Mascotas/index.php');
    }
    
     public function Del(){
        $data = new mascota();
        
        $data->Id_mascota = $_REQUEST['Id_mascota'];
        $data->ID_Cliente = $_REQUEST['ID_Cliente'];
        $data->Nombre_mascota = $_REQUEST['Nombre_mascota'];
        $data->Tipo_mascota= $_REQUEST['Tipo_mascota'];
        $data->fecha_nacimiento = $_REQUEST['fecha_nacimiento'];
        $data->Sexo = $_REQUEST['Sexo'];
        $data->Peso = $_REQUEST['Peso'];
        $data->Ultima_visita = $_REQUEST['Ultima_visita'];
         $data->Vacunas = $_REQUEST['Vacunas'];
          $data->Observaciones = $_REQUEST['Observaciones'];

        $data->Id_mascota > 0
            ? $this->model->del($data)
            : $this->model->update($data);
        header('Location: index.php');
    }
    
 
   
}


