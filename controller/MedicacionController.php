

<?php

require_once '../../model/Medicacion/Medicmodel.php';

class MedicController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Medic();
    }    
    public function Index(){
        require_once '../../view/Medicacion/Medic.php';
       
    }    
    public function Crud(){
        $data = new Medic();
        
        if(isset($_REQUEST['ID_medicamento'])){
            $data = $this->model->getByID($_REQUEST['ID_medicamento']);
            
        }       
        require_once '../../view/Medicacion/Editarmedicamento.php';          
    }  
    public function add(){
        $data = new Medic();
        
        $data->ID_medicamento = $_REQUEST['ID_medicamento'];
        $data->Medicacion = $_REQUEST['Medicacion'];
        $data->Precio = $_REQUEST['Precio'];
        

        $data->ID_medicamento > 0
            ? $this->model->update($data)
            : $this->model->add($data);
        header('Location: ../../view/Medicacion/index_1.php');
    }
    
     public function Del(){
        $data = new Medic();
        
         $data->ID_medicamento = $_REQUEST['ID_medicamento'];
        $data->Medicacion = $_REQUEST['Medicacion'];
        $data->Precio = $_REQUEST['Precio'];

        $data->ID_medicamento > 0
            ? $this->model->del($data)
            : $this->model->update($data);
        header('Location: index_1.php');
    }
    
 
   
}

