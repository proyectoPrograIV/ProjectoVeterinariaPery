

<?php

require_once '../../model/Visita/Visitamodel.php';

class VisitaController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Visita();
    }    
    public function Index(){
        require_once '../../view/Visita/Visita.php';
       
    }    
    public function Crud(){
        $data = new Visita();
        
        if(isset($_REQUEST['Id_Visitas'])){
            $data = $this->model->getByID($_REQUEST['Id_Visitas']);
            
        }       
        require_once '../../view/Visita/Editarvisita.php';          
    }  
    public function add(){
        $data = new Visita();
        
        $data->Id_Visitas = $_REQUEST['Id_Visitas'];
        $data->Id_mascota = $_REQUEST['Id_mascota'];
        $data->Fecha_inscripcion = $_REQUEST['Fecha_inscripcion'];
		$data->Forma_pago = $_REQUEST['Forma_pago'];
		$data->Cantidad_pagada = $_REQUEST['Cantidad_pagada'];
		$data->IVA = $_REQUEST['IVA'];
		$data->Descripcion = $_REQUEST['Descripcion'];
        

        $data->Id_Visitas > 0
            ? $this->model->update($data)
            : $this->model->add($data);
        header('Location: ../../view/Visita/index.php');
    }
    
     public function Del(){
        $data = new Visita();
        
         $data->Id_Visitas = $_REQUEST['Id_Visitas'];
        $data->Id_mascota = $_REQUEST['Id_mascota'];
        $data->Fecha_inscripcion = $_REQUEST['Fecha_inscripcion'];
		$data->Forma_pago = $_REQUEST['Forma_pago'];
		$data->Cantidad_pagada = $_REQUEST['Cantidad_pagada'];
		$data->IVA = $_REQUEST['IVA'];
		$data->Descripcion = $_REQUEST['Descripcion'];
                
        $data->Id_Visitas > 0
            ? $this->model->del($data)
            : $this->model->update($data);
        header('Location: index.php');
    }
    
 
   
}

