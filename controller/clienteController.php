

<?php

require_once '../../model/clientes/Clientmodel.php';


class ClientController{
    
    private $model;
    
    public function __construct(){
        $this->model = new Client();
    }    
    public function Index(){
        require_once '../../view/clientes/clients.php';
        //primer punto directorio actual , la salida al siguiente directorio , slash para poder ubicarte en el generak , y el ultimo para dar entrada y acceder al directorio.
       
    }    
    public function Crud(){
        $data = new Client();
        
        if(isset($_REQUEST['ID_Cliente'])){
            $data = $this->model->getByID($_REQUEST['ID_Cliente']);
            
        }       
        require_once '../../view/clientes/EditarCliente.php';          
    }  
    public function add(){
        $data = new Client();
        
        $data->ID_Cliente = $_REQUEST['ID_Cliente'];
        $data->Nombre = $_REQUEST['Nombre'];
        $data->Apellido = $_REQUEST['Apellido'];
        $data->Direccion= $_REQUEST['Direccion'];
        $data->Telefono = $_REQUEST['Telefono'];
        $data->Fecha_ultima_visita = $_REQUEST['Fecha_ultima_visita'];
        $data->Descuento = $_REQUEST['Descuento'];
        $data->Saldo = $_REQUEST['Saldo'];

        $data->ID_Cliente > 0
            ? $this->model->update($data)
            : $this->model->add($data);
        header('Location: ../../view/clientes/index.php');
    }
    
     public function Del(){
        $data = new Client();
        
        $data->ID_Cliente = $_REQUEST['ID_Cliente'];
        $data->Nombre = $_REQUEST['Nombre'];
        $data->Apellido = $_REQUEST['Apellido'];
        $data->Direccion= $_REQUEST['Direccion'];
        $data->Telefono = $_REQUEST['Telefono'];
        $data->Fecha_ultima_visita = $_REQUEST['Fecha_ultima_visita'];
        $data->Descuento = $_REQUEST['Descuento'];
        $data->Saldo = $_REQUEST['Saldo'];

        $data->ID_Cliente > 0
            ? $this->model->del($data)
            : $this->model->update($data);
        header('Location: index.php');
    }
    
 
   
}


