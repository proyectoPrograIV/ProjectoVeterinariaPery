<?php


?>

<head>
    <title>CRUD Mascotas</title> 
</head>

<body bgcolor="LightBlue">
<center><h1 class="page-header">Mascotas de la veterinaria</h1></center>

<center><div>
    <a href="?controller=mascota&accion=Crud">Agregar Mascotas</a>
    
    </div></center>
<br>

<center><table class="table table-striped">
     <thead>
        <tr>
            <th>ID_Mascotas</th>
            <th>ID_Cliente</th>
            <th>Nombre de la Mascotas</th>
            <th>Tipo de Mascota</th>
            <th>Fecha de Nacimiento</th>
            <th>Sexo</th>
            <th>Peso</th>
            <th>Ultima_Visita</th>
			<th>Vacunas</th>
			<th>Observaciones</th>
            <th ></th>
            <th ></th>
        </tr>
        </thead>
        
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	    <td><?php echo $r->Id_mascota; ?></td>
            <td><?php echo $r->ID_Cliente; ?></td>
            <td><?php echo $r->Nombre_mascota; ?></td> 
            <td><?php echo $r->Tipo_mascota; ?></td>
            <td><?php echo $r->fecha_nacimiento; ?></td>
            <td><?php echo $r->Sexo; ?></td>
            <td><?php echo $r->Peso; ?></td>
            <td><?php echo $r->Ultima_visita; ?></td>
			<td><?php echo $r->Vacunas; ?></td>
			<td><?php echo $r->Observaciones; ?></td>
            <td>
                <a href="?controller=mascota&accion=Crud&Id_mascota=<?php echo $r->Id_mascota; ?>">Editar Mascota</a>
            </td>
            <td>
               <a href="?controller=mascota&accion=Del&Id_mascota=<?php echo $r->Id_mascota; ?>">Eliminar Mascota</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 

