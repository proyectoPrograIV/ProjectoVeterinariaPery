<!DOCTYPE html>
<html>
    <head>
        <title>Cliente MVC</title>
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    </head>
    <body>



<?php
require_once "../../controller/clienteController.php";

//instancia del controlador
$controller = new ClientController();


if(!isset($_REQUEST['controller']))
{      	
    $controller->Index();    
}else{
    $control = ($_REQUEST['controller']);
    $accion = isset($_REQUEST['accion']) ? $_REQUEST['accion'] : 'Index';
    
       
    // Llama la accion
    call_user_func(array($controller, $accion));
    
}