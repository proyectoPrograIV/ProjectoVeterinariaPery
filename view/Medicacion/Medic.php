

<?php


?>

<head>
    <title>CRUD Medicamentos</title> 
</head>
<br>
<body bgcolor="LightBlue">
<center><h1 class="page-header">Medicamentos para mascotas</h1></center>

<center><div>
    <a href="?controller=Medic&accion=Crud">Agregar Medicamento</a>
    
    </div></center>
<br>

<center><table class="table table-striped">
     <thead>
        <tr>
            <th >ID_medicamento</th>
            <th >Medicacion</th>
            <th>Precio</th>
            <th ></th>
            <th ></th>
        </tr>
        </thead>
        
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
      
        <tr>
	    <td><?php echo $r->ID_medicamento; ?></td>
            <td><?php echo $r->Medicacion; ?></td>
            <td><?php echo $r->Precio; ?></td>
            <td>
        
                <a href="?controller=Medic&accion=Crud&ID_medicamento=<?php echo $r->ID_medicamento; ?>">Editar Medicamento</a>
            </td>
            <td>
               <a href="?controller=Medic&accion=Del&ID_medicamento=<?php echo $r->ID_medicamento; ?>">Eliminar Medicamento</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 