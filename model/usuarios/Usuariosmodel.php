<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once("../../model/usuarios/database_us.php");

class Usuarios{
    
    private $pdo;    
    public $Id_Usuario;
    public $Usuario;
    public $Password;
	public $Privilegio;
   

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT Id_Usuario, Usuario, Password, Privilegio FROM usuarios");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($Id_Usuario)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT  Id_Usuario, Usuario, Password, Privilegio FROM usuarios WHERE Id_Usuario = ?");
			          

			$stm->execute(array($Id_Usuario));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM usuarios WHERE Id_Usuario = ?");			          

			$stm->execute(array($data->Id_Usuario));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE usuarios SET 
						Usuario = ?, 
						Password = ?
						Privilegio = ?
                                            
				    WHERE Id_Usuario = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Usuario, 
                        $data->Password,
                        $data->Privilegio        
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add($data)
	{
		try{
		$sql = "INSERT INTO usuarios(Usuario,Password,Privilegio) 
		        VALUES (?, ?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                     $data->Usuario, 
                     $data->Password,
					 $data->Privilegio
                  
                    
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}