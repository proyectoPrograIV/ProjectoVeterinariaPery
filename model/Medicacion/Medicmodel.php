<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once("../../model/Medicacion/database_1.php");

class Medic{
    
    private $pdo;    
    public $ID_medicamento;
    public $Medicacion;
    public $Precio;
   

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT ID_medicamento, Medicacion, Precio FROM medicacion");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($ID_medicamento)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT  ID_medicamento, Medicacion, Precio FROM medicacion WHERE ID_medicamento = ?");
			          

			$stm->execute(array($ID_medicamento));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM medicacion WHERE ID_medicamento = ?");			          

			$stm->execute(array($data->ID_medicamento));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE medicacion SET 
						Medicacion = ?, 
						Precio = ?
                                            
				    WHERE ID_medicamento = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Medicacion, 
                        $data->Precio,
                        $data->ID_medicamento        
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add($data)
	{
		try{
		$sql = "INSERT INTO medicacion(Medicacion,Precio) 
		        VALUES (?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                     $data->Medicacion, 
                     $data->Precio
                  
                    
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}