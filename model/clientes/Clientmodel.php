<?php


include_once("../../model/clientes/database.php");

class Client{
    
    private $pdo;    
    public $ID_Cliente;
    public $Nombre;
    public $Apellido;
    public $Direccion;
    public $Telefono;
    public $Fecha_ultima_visita;
    public $Descuento;
    public $Saldo;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT ID_Cliente, Nombre, Apellido, Direccion, Telefono, Fecha_ultima_visita, Descuento, Saldo FROM clientes");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($ID_Cliente)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT  Nombre, Apellido, Direccion, Telefono, Fecha_ultima_visita, Descuento, Saldo FROM clientes WHERE ID_Cliente = ?");
			          

			$stm->execute(array($ID_Cliente));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM clientes WHERE ID_Cliente = ?");			          

			$stm->execute(array($data->ID_Cliente));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE clientes SET 
						Nombre = ?, 
						Apellido = ?,
                                                Direccion = ?,
                                                Telefono = ?,
                                                Fecha_ultima_visita = ?,
                                                Descuento = ?,
                                                Saldo = ?
				    WHERE ID_Cliente = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Nombre, 
                        $data->Apellido,
                        $data->Direccion,
                        $data->Telefono,
                        $data->Fecha_ultima_visita,
                        $data->Descuento,
                        $data->Saldo,
                        $data->ID_Cliente         
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add($data)
	{
		try{
		$sql = "INSERT INTO clientes(Nombre,Apellido,Direccion,Telefono,Fecha_ultima_visita,Descuento,Saldo) 
		        VALUES (?, ?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                     $data->Nombre, 
                     $data->Apellido,
                     $data->Direccion,
                     $data->Telefono,
                     $data->Fecha_ultima_visita,
                     $data->Descuento,
                     $data->Saldo
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}