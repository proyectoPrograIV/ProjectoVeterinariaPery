<?php


include_once("../../model/Mascota/database_M.php");

class mascota{
    
    private $pdo;    
    public $Id_mascota;
    public $ID_Cliente;
    public $Nombre_mascota;
    public $Tipo_mascota;
    public $fecha_nacimiento;
    public $Sexo;
    public $Peso;
    public $Ultima_visita;
	public $Vacunas;
	public $Observaciones;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT Id_mascota, ID_Cliente, Nombre_mascota, Tipo_mascota, fecha_nacimiento, Sexo, Peso, Ultima_visita, Vacunas, Observaciones FROM mascota");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	 public function getByID($Id_mascota)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT  ID_Cliente, Nombre_mascota, Tipo_mascota, fecha_nacimiento, Sexo, Peso, Ultima_visita, Vacunas, Observaciones FROM mascota WHERE Id_mascota = ?");
			          

			$stm->execute(array($Id_mascota));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
	

 	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM mascota WHERE Id_mascota = ?");			          

			$stm->execute(array($data->Id_mascota));
		} catch (Exception $e){
			die($e->getMessage());
		} 
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE mascota SET 
						Nombre_mascota = ?, 
						Tipo_mascota = ?,
                                                fecha_nacimiento = ?,
                                                Sexo = ?,
                                                Peso = ?,
                                                Ultima_visita = ?,
                                                Vacunas = ?
				    WHERE Id_mascota = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Id_mascota, 
                        $data->ID_Cliente,
                        $data->Nombre_mascota,
                        $data->Tipo_mascota,
                        $data->fecha_nacimiento,
                        $data->Sexo,
                        $data->Peso,
                        $data->Ultima_visita,
                        $data->vacunas,
                        $data->Observaciones   						
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add($data)
	{
		try{
		$sql = "INSERT INTO mascota(ID_Cliente,Nombre_mascota,Tipo_mascota,fecha_nacimiento,Sexo,Peso,Ultima_visita,Vacunas,Observaciones) 
		        VALUES (?, ?,?,?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                  
                        $data->ID_Cliente,
                        $data->Nombre_mascota,
                        $data->Tipo_mascota,
                        $data->fecha_nacimiento,
                        $data->Sexo,
                        $data->Peso,
                        $data->Ultima_visita,
                        $data->Vacunas,
                        $data->Observaciones   						
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	} 
}