<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once("../../model/servicios/database_S.php");

class Servicios{
    
    private $pdo;    
    public $ID_servicio;
    public $Tipo_servicio;
   

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT ID_servicio, Tipo_servicio FROM servicios");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($ID_servicio)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT  ID_servicio, Tipo_servicio FROM servicios WHERE ID_servicio = ?");
			          

			$stm->execute(array($ID_servicio));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM servicios WHERE ID_servicio = ?");			          

			$stm->execute(array($data->ID_servicio));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE servicios SET 
						Tipo_servicio = ?, 
                                            
				    WHERE ID_servicio = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Tipo_servicio, 
                        $data->ID_servicio        
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add($data)
	{
		try{
		$sql = "INSERT INTO servicios(Tipo_servicio) 
		        VALUES (?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                     $data->Tipo_servicio, 
                  
                  
                    
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}