<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once("../../model/Visita/database_V.php");

class Visita{
    
    private $pdo;    
    public $Id_Visitas;
    public $Id_mascota;
    public $Fecha_inscripcion;
	public $Forma_pago;
	public $Cantidad_pagada;
	public $IVA;
	public $Descripcion;
   

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT Id_Visitas, Id_mascota, Fecha_inscripcion, Forma_pago, Cantidad_pagada, IVA, Descripcion FROM visitas");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function getByID($Id_Visitas)
	{
		try{
			$stm = $this->pdo
			          ->prepare("SELECT  Id_mascota, Fecha_inscripcion, Forma_pago, Cantidad_pagada, IVA, Descripcion FROM visitas WHERE Id_Visitas = ?");
			          

			$stm->execute(array($Id_Visitas));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function del($data)
	{
		try{
			$stm = $this->pdo
			            ->prepare("DELETE FROM visitas WHERE ID_Visitas = ?");			          

			$stm->execute(array($data->Id_Visitas));
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function update($data)
	{
		try{
			$sql = "UPDATE visitas SET 
						Id_mascota = ?, 
						Fecha_inscripcion = ?,
						Forma_pago = ?,
						Cantidad_pagada = ?,
						IVA = ?,
						Descripcion = ?
                                            
				    WHERE Id_Visitas = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                     $data->Id_mascota,
					 $data->Fecha_inscripcion, 
                     $data->Forma_pago,
					 $data->Cantidad_pagada, 
                     $data->IVA,
					 $data->Descripcion,
					 $data->Id_Visitas
					)
				);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}

	public function add($data)
	{
		try{
		$sql = "INSERT INTO visitas(Id_mascota,Fecha_inscripcion,Forma_pago,Cantidad_pagada,IVA,Descripcion) 
		        VALUES (?,?,?,?,?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                     
                     $data->Id_mascota,
					 $data->Fecha_inscripcion, 
                     $data->Forma_pago,
					 $data->Cantidad_pagada, 
                     $data->IVA,
					 $data->Descripcion
                  
                    
                )
			);
		} catch (Exception $e){
			die($e->getMessage());
		}
	}
}