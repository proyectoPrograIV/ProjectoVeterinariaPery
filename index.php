
<?php
session_start();
    require_once("Login/usuario.php");
    $usuario = new Usuario();

    if($usuario->is_loggedin()!="")
    {
            $usuario->redirect('home.php');
    }

    if(isset($_POST['login']))
    {
            $nom = $_POST['nomcorreo'];
            $correo = $_POST['nomcorreo'];
            $clave = $_POST['clave'];

            if($usuario->Login($nom,$correo,$clave))
            {
                    $usuario->redirect('home.php');
            }
            else
            {
                $error = "Datos Erroneos!";
            }	
    }
?>
<html>
    <head>
        <!-- Bootstrap Core CSS -->
        <link href="assets/css1/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Custom CSS -->
        <link href="assets/css1/business-casual.css" rel="stylesheet">

        <!-- Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
 
    </head>
    <body background="assets/Gatosencasa_huellasdevinilo.jpg">

    <div class="signin-form">

            <div class="container">


           <form class="form-signin" method="post" id="login-form">

               <h1 style="color:black"><center>Inicio de Sesion</center</h1><hr />

            <div id="error">
            <?php
                            if(isset($error))
                            {
                                    ?>
                    <div class="alert alert-danger">
                       <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?> !
                    </div>
                    <?php
                            }
                    ?>
            </div>
               
               <center><img src="assets/mas.jpg" width="200"  height="200"/></center>
               <br>
            <div class="form-group">
            <input type="text" class="form-control" name="nomcorreo" placeholder="Usuario o Correo" required />            
            </div>

            <div class="form-group">
            <input type="password" class="form-control" name="clave" placeholder="Su Clave" required/>
            </div>

            <hr />

            <div class="form-group">
                <button type="submit" name="login" class="btn btn-primary">
                    <i class="glyphicon glyphicon-log-in"></i> &nbsp; Iniciar Sesion
                </button>
            </div>  
          
            <label style="color:black"> No posee cuenta! <a href="registro.php">Registrarse</a></label>
          </form>

        </div>

    </div>

    </body>
</html>
