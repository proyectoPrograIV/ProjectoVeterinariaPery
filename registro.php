<?php
    session_start();
    require_once('Login/usuario.php');
    $usuario = new Usuario();

    if($usuario->is_loggedin()!="")
    {
            $usuario->redirect('home.php');
    }

    if(isset($_POST['registrar']))
    {
            $nombre = $_POST['nombre'];
            $correo = $_POST['correo'];
            $clave =  $_POST['clave'];	

            if($nombre=="")	{
                    $error[] = "Escriba un usuario!";	
            }
            else if($correo=="")	{
                    $error[] = "Escriba un correo!";	
            }
            else if(!filter_var($correo, FILTER_VALIDATE_EMAIL))	{
                $error[] = 'Escriba una direccion valida de correo!';
            }
            else if($clave=="")	{
                    $error[] = "Escriba la clave!";
            }
            else if(strlen($clave) < 6){
                    $error[] = "La clave debe contener al menos 6 caracteres";	
            }
            else
            {
                    try
                    {
                           //verificar si existe el usuario o el correo
                            $resultado=$usuario->ListarUsuarios($nombre, $correo);

                            if($resultado['nomUsuario']==$nombre) {
                                    $error[] = "El nombre de usuario ya existe, favor escriba otro";
                            }
                            else if($resultado['emailUsuario']==$correo) {
                                    $error[] = "El correo ya esta registrado, favor escriba otra direccion";
                            }
                            else
                            {
                                    if($usuario->registrar($nombre,$correo,$clave)){	
                                            $usuario->redirect('registro.php?registrado');
                                    }
                            }
                    }
                    catch(PDOException $e)
                    {
                            echo $e->getMessage();
                    }
            }	
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Registro Usuario</title>
        <!-- Bootstrap Core CSS -->
    <link href="assets/css1/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Custom CSS -->
    <link href="assets/css1/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
 
    </head>
    <body background="http://4.bp.blogspot.com/-TUm1XWvLIVE/Tee92D_oX6I/AAAAAAAACHs/HlDavR6SWxk/s1600/Gatosencasa_huellasdevinilo.jpg">
    
<center>
    <form method="post" class="form">
        <h2 style="color:black">Registro de Usuario</h2><hr />
        <br>
            <center> <img src="https://www.sura.com/blogs/images1/tener-mascotas-bueno-salud-mental-emocional.jpg" height="150" width="200"/></center>
            <br>
        <?php
        if (isset($error)) {
            foreach ($error as $error) {
                ?>
                <div class="alert alert-danger">
                    <i class="glyphicon glyphicon-warning-sign"></i> &nbsp; <?php echo $error; ?>
                </div>
        <?php
            }
        } else if (isset($_GET['registrado'])) {
            ?>
            <div class="alert alert-info">
                <i class="glyphicon glyphicon-log-in"></i> &nbsp; Registro Exitoso <a href='index.php'>Iniciar sesion</a> aquí
            </div>
            <?php
        }
        ?>
        <div class="form-group">
            
            <input type="text" class="form-control" name="nombre" placeholder="Escriba su nombre" value="<?php if (isset($error)) {
            echo $nombre;
        } ?>" />
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="correo" placeholder="Escriba su correo" value="<?php if (isset($error)) {
            echo $correo;
        } ?>" />
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="clave" placeholder="Escriba su clave" />
        </div>
        <div class="clearfix"></div><hr />
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="registrar">
                <i class="glyphicon glyphicon-check"></i>&nbsp;Registrar
            </button>
        </div>
        <label style="color:black">Ya posee una cuenta! <a href="index.php" style="color:blue">Iniciar Sesion</a></label>
    </form>
               
       

</body>
</html>
